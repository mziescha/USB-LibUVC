#define PERL_NO_GET_CONTEXT
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "ppport.h"

#include <libuvc/libuvc.h>

#include "const-c.inc"  

typedef uvc_context_t *USB__LibUVC__XS;
typedef uvc_error_t   *USB__LibUVC__XS__Error;
typedef uvc_device_t  *USB__LibUVC__XS__Device;

static SV *
pointer_object(pTHX_ const char *class_name, void *pv)
{
    SV *rv = newSV(0);
    sv_setref_pv(rv, class_name, pv);
    return rv;
}

// Finds a camera identified by vendor, product and/or serial number. More...
// uvc_error_t
// uvc_find_device (uvc_context_t *ctx, uvc_device_t **dev, int vid, int pid, const char *sn)
 
// Get the number of the bus to which the device is attached.
// uint8_t
// uvc_get_bus_number (uvc_device_t *dev)
     
// Get the number assigned to the device within its bus.
// uint8_t
// uvc_get_device_address (uvc_device_t *dev)
     
// Open a UVC device. More...
// uvc_error_t
// uvc_open (uvc_device_t *dev, uvc_device_handle_t **devh)
     
// Get a descriptor that contains the general information about a deviceFree *desc with uvc_free_device_descriptor when you are done. More...
// uvc_error_t
// uvc_get_device_descriptor (uvc_device_t *dev, uvc_device_descriptor_t **desc)
     
// Frees a device descriptor created with uvc_get_device_descriptor. More...
// void
// uvc_free_device_descriptor (uvc_device_descriptor_t *desc)
     
// Get a list of the UVC devices attached to the system. More...
// uvc_error_t
// uvc_get_device_list (uvc_context_t *ctx, uvc_device_t ***list)
     
// Frees a list of device structures created with uvc_get_device_list. More...
// void
// uvc_free_device_list (uvc_device_t **list, uint8_t unref_devices)
     
// Get the uvc_device_t corresponding to an open device. More...
// uvc_device_t*
// uvc_get_device (uvc_device_handle_t *devh)
     
// Get the underlying libusb device handle for an open deviceThis can be used to access other interfaces on the same device, e.g. More...
// libusb_device_handle*
// uvc_get_libusb_handle (uvc_device_handle_t *devh)
     
// Increment the reference count for a device. More...
// void
// uvc_ref_device (uvc_device_t *dev)

// Close a device. More...
// void
// uvc_close (uvc_device_handle_t *devh)

// Set a callback function to receive status updates. 
// void
// uvc_set_status_callback (uvc_device_handle_t *devh, uvc_status_callback_t cb, void *user_ptr)

//void
//uvc_exit(uvc_context_t *ctx)

MODULE = USB::LibUSB::XS      PACKAGE = USB::LibUSB::XS
INCLUDE: const-xs.inc
  
######## Library initialization/deinitialization ##############################
MODULE = USB::LibUVC::XS        PACKAGE = USB::LibUVC::XS     PREFIX = libuvc_

void
libuvc_init(char *class)
PPCODE:
    uvc_context_t *ctx;
    int rv = uvc_init(&ctx, NULL);
    mXPUSHi(rv);
    if (rv == 0)
        mXPUSHs(pointer_object(aTHX_ class, ctx));

# void
# DESTROY(USB::LibUVC::XS ctx)
# CODE:
#     do_not_warn_unused(ctx);

