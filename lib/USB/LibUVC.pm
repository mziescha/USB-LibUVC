package USB::LibUVC;

use 5.026001;
use Moo;
use Carp;
use strict;
use warnings;
use USB::LibUVC::XS;

#use USB::LibUVC::Device;
#use USB::LibUVC::Device::Handle;

# Export USB::LibUVC::XS constants
use Exporter 'import';
our @EXPORT = @USB::LibUVC::XS::EXPORT;

our $VERSION = '0.01';


sub BUILD {
    my ($self, @args) = @_;

    my ($rv, $ctx) = USB::LibUSB::XS->init();
    $self->_handle_error($rv, "init");
    $self->_ctx($ctx);
}

sub init {
    return new(@_);
}


# Preloaded methods go here.

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

USB::LibUVC - Perl extension for blah blah blah

=head1 SYNOPSIS

  use USB::LibUVC;
  blah blah blah

=head1 DESCRIPTION

Stub documentation for USB::LibUVC, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

Mario Zieschang, E<lt>mziescha@E<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2018 by Mario Zieschang

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.26.1 or,
at your option, any later version of Perl 5 you may have available.


=cut
